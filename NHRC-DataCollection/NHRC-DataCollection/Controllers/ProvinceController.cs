﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NHRC_DataCollection.Data;
using NHRC_DataCollection.Models;

namespace NHRC_DataCollection.Controllers
{
    public class ProvinceController : Controller
    {
        ApplicationDbContext _applicationDbContext;
        public ProvinceController(ApplicationDbContext context)
        {
            _applicationDbContext = context;
        }
        // GET: Province
        public ActionResult Index()
        {
            return View();
        }

        // GET: Province/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Province/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Province/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProvinceModel provinceModel)
        {
            if (!ModelState.IsValid)
            {
                try
                {
                    _applicationDbContext.Add(provinceModel);
                    _applicationDbContext.SaveChanges();

                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    return View();
                }
            }
            else return View(provinceModel);
           
        }

        // GET: Province/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Province/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Province/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Province/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}