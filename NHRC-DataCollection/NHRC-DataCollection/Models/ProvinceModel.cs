﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NHRC_DataCollection.Models
{
    public class ProvinceModel
    {
        [Key]
        public int ProvinceId { get; set; }
        [Required]
        [StringLength(200,MinimumLength = 4)]
        public string Name { get; set; }
    }
}
